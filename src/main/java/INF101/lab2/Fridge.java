package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	ArrayList<FridgeItem> items = new ArrayList<FridgeItem>(20);

	public Fridge() {
		items = new ArrayList <FridgeItem>(20);
	}
	
	int max_size = 20;
	
	public int totalSize() {
		return max_size;
	}

	@Override
	public int nItemsInFridge() {
		return this.items.size();
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (items.size() == totalSize())
		return false;
		return items.add(item);
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (!items.contains(item)) {
			throw new NoSuchElementException("Item not in fridge...");
		}
		items.remove(item);
	}

	@Override
	public void emptyFridge() {
		items.removeAll(items);
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List <FridgeItem> expired = expiredItems();
		items.removeAll(expired);
		return expired;
	}

	private List<FridgeItem> expiredItems() {
		List <FridgeItem> expired = new ArrayList <FridgeItem>();
		for (FridgeItem item : items) {
			if (item.hasExpired()) {
				expired.add(item);
			}
		}
		return expired;
	}

}